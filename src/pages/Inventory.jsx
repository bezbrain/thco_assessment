import { MainHeader } from "../components/general";

const Inventory = () => {
  return (
    <div>
      <MainHeader />
      <h1>Inventory Page</h1>
    </div>
  );
};

export default Inventory;
