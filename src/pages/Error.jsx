import { MainHeader } from "../components/general";

const Error = () => {
  return (
    <div>
      <MainHeader />
      <h1>Error Page</h1>
    </div>
  );
};

export default Error;
