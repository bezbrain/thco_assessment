import { MainHeader } from "../components/general";
import { Chat, Header, Contact } from "../components/conversations";

const Conversations = () => {
  return (
    <div>
      <MainHeader />
      <section className="conversation-body">
        <Header />
        <div className="contact-con">
          <Contact />
          <Chat />
        </div>
      </section>
    </div>
  );
};

export default Conversations;
