import { Events, Charts, RecentOrders } from "../components/dashboard";

import { MainHeader } from "../components/general";

const Dashboard = () => {
  return (
    <main>
      <MainHeader />
      <section className="dasboard-body">
        <Events />
        <div className="charts-and-order">
          <Charts />
          <RecentOrders />
        </div>
      </section>
    </main>
  );
};

export default Dashboard;
