import Dashboard from "./Dashboard";
import Orders from "./Orders";
import Customers from "./Customers";
import Inventory from "./Inventory";
import Conversations from "./Conversations";
import Settings from "./Settings";
import ContactSupport from "./ContactSupport";
import Gift from "./Gift";
import SharedLatout from "./SharedLatout";
import Error from "./Error";

export {
  Dashboard,
  Orders,
  Customers,
  Inventory,
  Conversations,
  Settings,
  ContactSupport,
  Gift,
  SharedLatout,
  Error,
};
