import { MainHeader } from "../components/general";

const Customers = () => {
  return (
    <div>
      <MainHeader />
      <h1>Customers Page</h1>
    </div>
  );
};

export default Customers;
