import { Outlet } from "react-router-dom";
import { SideBar, LogoutNoti } from "../components/general";

const SharedLatout = () => {
  return (
    <main className="dashboard">
      <SideBar />
      <div className="outlet-con">
        <Outlet />
      </div>
      <LogoutNoti />
    </main>
  );
};

export default SharedLatout;
