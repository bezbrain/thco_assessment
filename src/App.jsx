import { BrowserRouter, Routes, Route } from "react-router-dom";
import {
  Dashboard,
  Orders,
  Customers,
  Inventory,
  Conversations,
  Settings,
  ContactSupport,
  Gift,
  SharedLatout,
  Error,
} from "./pages";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<SharedLatout />}>
          <Route index element={<Dashboard />} />
          <Route path="/orders" element={<Orders />} />
          <Route path="/customers" element={<Customers />} />
          <Route path="/inventory" element={<Inventory />} />
          <Route path="/conversations" element={<Conversations />} />
          <Route path="/settings" element={<Settings />} />
          <Route path="/contact-support" element={<ContactSupport />} />
          <Route path="/gift" element={<Gift />} />
          <Route path="*" element={<Error />} />
        </Route>
      </Routes>
      {/* Library to handle pop up notification */}
      <ToastContainer position="top-center" className="custom-toast" />
    </BrowserRouter>
  );
};

export default App;
