import { FaCaretDown } from "react-icons/fa";
import styles from "../../styles/general/header.module.css";
import { useLocation, useNavigate } from "react-router-dom";
import { sideBarData } from "../../data";

const MainHeader = () => {
  const navigate = useNavigate();
  const location = useLocation();

  // Get Index of pathname
  const findSideBar = sideBarData.findIndex(
    (each) => each.link === location.pathname
  );

  // Filter out fist 6 indices only
  const filterIndex = () => {
    if (findSideBar > 5 || findSideBar < 0) {
      return 0;
    }
    return findSideBar;
  };

  // Capitalize Pathname
  const extPathname = sideBarData[filterIndex()].link;
  const capitalizedPathname =
    extPathname.charAt(1).toUpperCase() + extPathname.slice(2);

  return (
    <header className={styles.header}>
      <div>
        <p>{sideBarData[filterIndex()].sideBarContent}</p>
        <img
          src="../../icons/Home.png"
          alt="Home"
          onClick={() => navigate("/")}
        />
        <span> / {capitalizedPathname}</span>
      </div>
      <section className={styles.rightSide}>
        <button>
          Nanny's Shop <FaCaretDown />
        </button>
        <img src="../../icons/Notification.png" alt="Notification" />
        <img src="../../icons/profile_1.png" alt="profile" />
      </section>
    </header>
  );
};

export default MainHeader;
