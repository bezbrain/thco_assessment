import { useNavigate } from "react-router-dom";
import styles from "../../styles/general/logo.module.css";

const Logo = () => {
  const navigate = useNavigate();

  return (
    <div className={styles.div}>
      <img
        src="../../icons/Logo.png"
        alt="Metrix"
        onClick={() => navigate("/")}
      />
      <img
        src="../../icons/Sub_Logo.png"
        alt="Logo"
        onClick={() => navigate("/")}
      />
    </div>
  );
};

export default Logo;
