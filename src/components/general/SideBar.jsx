import Logo from "./Logo";
import { Link, useLocation } from "react-router-dom";
import { sideBarData } from "../../data";
import styles from "../../styles/general/sideBar.module.css";
import { useGlobalContext } from "../../context";

const SideBar = () => {
  const { setIsModal, isLogout } = useGlobalContext();

  const location = useLocation();

  return (
    <main className={styles.main}>
      <aside>
        <Logo />
        <nav>
          {/* Upper Nav */}
          <ul className={styles.upperNav}>
            {sideBarData.map((each) => {
              const { id, icon, sideBarContent, link } = each;
              return (
                <Link to={link} key={id}>
                  <li
                    className={`${styles.addWidth} ${
                      location.pathname === link ? styles.active : ""
                    } `}
                    title={sideBarContent}
                  >
                    <img
                      src={icon}
                      alt={icon}
                      className={`${
                        location.pathname !== "/" && id === 1
                          ? styles.dashboardIconBg
                          : ""
                      }`}
                    />
                    <p className={styles.addSideBarContent}>{sideBarContent}</p>
                  </li>
                </Link>
              );
            })}
          </ul>
          {/* Lower Nav */}
          <ul className={styles.lowerNav}>
            <Link to="/contact-support">
              <li className={styles.addWidth} title="Contact Support">
                <img src="../../../icons/headphones.png" alt="Contact" />
                <p className={styles.addSideBarContent}>Contact Support</p>
              </li>
            </Link>
            <div className={`${styles.giftCon} ${styles.addWidth}`}>
              <Link to="gift">
                <li title="Gift">
                  <img src="../../../icons/gift.png" alt="Gift" />
                  <p className={styles.addSideBarContent}>
                    Free Gift Awaits You!
                  </p>
                </li>
              </Link>
              <p className={styles.addSideBarContent}>Upgrade your account</p>
            </div>
            <li title={isLogout} onClick={() => setIsModal(true)}>
              <img src="../../../icons/Logout.png" alt="Logout" />
              <p className={styles.addSideBarContent}>{isLogout}</p>
            </li>
          </ul>
        </nav>
      </aside>
    </main>
  );
};

export default SideBar;
