import { useGlobalContext } from "../../context";
import { toast } from "react-toastify";
import styles from "../../styles/general/logout.module.css";
import { useNavigate } from "react-router-dom";

const LogoutNoti = () => {
  const {
    isModal,
    setIsModal,
    setIsLogout,
    isLogout,
    toggleLogout,
    setToggleLogout,
  } = useGlobalContext();

  const navigate = useNavigate();

  const handleLogoutConfirm = () => {
    if (isLogout === "Logout") {
      toast.success("You are Logged Out");
      setIsModal(false);
      navigate("/");
      setIsLogout("Login");
      setToggleLogout(true);
    } else {
      console.log("Pls login");
    }
  };

  return (
    <main
      className={`${styles.main} ${
        isModal ? styles.addScale : styles.removeScale
      }`}
    >
      <section>
        {!toggleLogout && (
          <div>
            <h3>Are you sure you want to Logout?</h3>
            <button onClick={() => setIsModal(false)}>Cancel</button>
            <button onClick={handleLogoutConfirm}>Yes</button>
          </div>
        )}
        {toggleLogout && (
          <div>
            <h3>Please Login</h3>
            <button onClick={() => setIsModal(false)}>Cancel</button>
          </div>
        )}
      </section>
    </main>
  );
};

export default LogoutNoti;
