import { FaCaretDown } from "react-icons/fa";
import styles from "../../styles/dashboard/upper-chart.module.css";

const UpperChart = () => {
  return (
    <section className={styles.section}>
      {/* Left Chart */}
      <div className={styles.leftChart}>
        <div className={styles.header}>
          <p>Marketing</p>
          <p>
            This Week <FaCaretDown />
          </p>
        </div>
        <div>
          <img
            src="../../icons/Dashboard_Icons/acquisition.png"
            alt="Acquisition"
          />
          <img src="../../icons/Dashboard_Icons/purchase.png" alt="Purchase" />
          <img
            src="../../icons/Dashboard_Icons/retention.png"
            alt="Retention"
          />
        </div>
        <div>
          <img src="../../icons/Dashboard_Icons/Upper_chart.png" alt="Chart" />
        </div>
      </div>

      {/* Right Chart */}
      <div className={styles.rightChart}>
        <div>
          <img src="../../icons/Dashboard_Icons/All_products_icon.png" alt="" />
          <div>
            <img
              src="../../icons/Dashboard_Icons/All_products.png"
              alt="Summary"
            />
          </div>
        </div>
        <div>
          <div>
            <img src="../../icons/Dashboard_Icons/Cart.png" alt="Cart" />
            <p>
              This Week <FaCaretDown />
            </p>
          </div>
          <img src="../../icons/Dashboard_Icons/Abadon_Cart.png" alt="Cart" />
        </div>
      </div>
    </section>
  );
};

export default UpperChart;
