import styles from "../../styles/dashboard/lower-chart.module.css";

const LowerChart = () => {
  return (
    <section className={styles.section}>
      <div>
        <img src="../../icons/Dashboard_Icons/bottom_graph.png" alt="Graph" />
      </div>
    </section>
  );
};

export default LowerChart;
