import { recentOrderData } from "../../data";
import styles from "../../styles/dashboard/order.module.css";

const RecentOrders = () => {
  return (
    <main className={styles.main}>
      <p>Recent Orders</p>
      {recentOrderData.map((each) => {
        const { id, status } = each;
        return (
          <section key={id}>
            <div className={styles.imgCon}>
              <img
                src={
                  status === "pending"
                    ? "../../icons/Dashboard_Icons/iPhone_1.png"
                    : "../../icons/Dashboard_Icons/iPhone_2.png"
                }
                alt="iPhone"
              />
            </div>
            <div className={styles.iPhoneCon}>
              <p>iPhone</p>
              <p>₦730,000.00 x 1</p>
            </div>
            <div className={styles.statusCon}>
              <p>12 Sept 2022</p>
              <button
                className={`${
                  status === "pending" ? styles.pending : styles.completed
                }`}
              >
                {status}
              </button>
            </div>
          </section>
        );
      })}
    </main>
  );
};

export default RecentOrders;
