import { LowerChart, UpperChart } from "../dashboard";

const Charts = () => {
  return (
    <main style={{ width: "100%" }}>
      <UpperChart />
      <LowerChart />
    </main>
  );
};

export default Charts;
