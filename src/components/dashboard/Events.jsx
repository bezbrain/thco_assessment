import { FaCaretDown } from "react-icons/fa";
import styles from "../../styles/dashboard/events.module.css";
import { eventData } from "../../data";
import { useGlobalContext } from "../../context";

const Events = () => {
  const { newOrder } = useGlobalContext();

  return (
    <main className={styles.main}>
      {eventData.map((each) => {
        const {
          id,
          icon,
          text1,
          text2,
          text3,
          figure1,
          figure2,
          figure3,
          increment,
          increment2,
        } = each;
        return (
          <section key={id}>
            <div className={styles.header}>
              <img src={icon} alt={text1} />
              <p>
                This Week <FaCaretDown />
              </p>
            </div>
            <div className={styles.footer}>
              <div>
                <p>{text1}</p>
                <p>
                  {figure1} <span>{increment}</span>
                </p>
              </div>
              <div>
                <p>{text2}</p>
                <p>
                  {`${text2 === "Pending" ? newOrder.length : figure2}`}{" "}
                  <span>{increment2}</span>
                </p>
              </div>
              <div>
                <p>{text3}</p>
                <p>{figure3}</p>
              </div>
            </div>
          </section>
        );
      })}
    </main>
  );
};

export default Events;
