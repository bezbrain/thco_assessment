import styles from "../../styles/conversations/recepient.module.css";

const Receipient = ({ text }) => {
  return (
    <main className={styles.main}>
      <div>
        <p>{text}</p>
      </div>
      <div>
        <p>12:57 am</p>
        <img src="../../../icons/Conversation_icons/read_icon.png" alt="Read" />
      </div>
    </main>
  );
};

export default Receipient;
