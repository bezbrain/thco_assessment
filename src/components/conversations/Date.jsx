import styles from "../../styles/conversations/date.module.css";

const Date = ({ text }) => {
  return (
    <section className={styles.date}>
      <p>{text}</p>
    </section>
  );
};

export default Date;
