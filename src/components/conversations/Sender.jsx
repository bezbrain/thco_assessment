import styles from "../../styles/conversations/sender.module.css";

const Sender = ({ text }) => {
  return (
    <main className={styles.main}>
      <div>
        <p>{text}</p>
      </div>
      <p>12:55 am</p>
    </main>
  );
};

export default Sender;
