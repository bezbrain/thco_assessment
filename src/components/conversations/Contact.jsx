import { FaSearch } from "react-icons/fa";
import styles from "../../styles/conversations/contact.module.css";
import { chatData } from "../../data";
import { useGlobalContext } from "../../context";

const Contact = () => {
  const { toggleContactList } = useGlobalContext();

  // Function to Dynamically add image
  const imageFunc = (img) => {
    if (img === "Jane Doe") {
      return "../../../images/Group 10.png";
    } else if (img === "Janet Adebayo") {
      return "../../../images/Rectangle 3.png";
    } else {
      return "../../../images/Group 12.png";
    }
  };

  return (
    <main
      className={`${styles.main} ${toggleContactList ? styles.addDisplay : ""}`}
    >
      <section>
        <div>
          <h2>Contacts</h2>
          <p>34</p>
        </div>
        <div className={styles.searchCon}>
          <input type="text" placeholder="Search" />
          <FaSearch className={styles.searchIcon} />
        </div>
      </section>
      {/* Contact List */}
      {chatData.map((each) => {
        const { id, name, status } = each;
        return (
          <div key={id} className={styles.people}>
            <img src={imageFunc(name)} alt="chat" />
            <div>
              <p>{name}</p>
              <p>Hi, I want to make enquiry about yo...</p>
            </div>
            <div>
              <p
                className={`${id > 4 ? styles.addStatus : ""} ${
                  id === 4 ? styles.addSingleStatus : ""
                }`}
              >
                {status}
              </p>
              <p>12:55am</p>
            </div>
          </div>
        );
      })}
    </main>
  );
};

export default Contact;
