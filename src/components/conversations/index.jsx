import Chat from "./Chat";
import Contact from "./Contact";
import Header from "./Header";
import Sender from "./Sender";
import Receipient from "./Receipient";
import Date from "./Date";
import TypeMsg from "./TypeMsg";

export { Chat, Contact, Header, Sender, Receipient, Date, TypeMsg };
