import styles from "../../styles/conversations/chat.module.css";
import { FaArrowRight, FaTimes } from "react-icons/fa";
import { Sender, Receipient, Date, TypeMsg } from "../conversations/";
import { useGlobalContext } from "../../context";

const Chat = () => {
  const { toggleContactList, setToggleContactList } = useGlobalContext();

  return (
    <main className={styles.main}>
      {/* Chat Header */}
      <header>
        <img src="../../../images/Group 10.png" alt="person" />
        <div className={styles.name}>
          <p>Jone Doe</p>
          <img src="../../../icons/Online_status.png" alt="online" />
        </div>
        <p>New Customer</p>
        <div className={styles.profile}>
          <p>View Profile</p>
          <div>
            <img src="../../../icons/Orders.png" alt="" />
            <span>0 Orders</span>
          </div>
        </div>
      </header>
      <hr />

      <Date text="12 August 2022" />

      {/* Chat Content section */}
      <section className={styles.chatContent}>
        <div className={styles.div}>
          <img src="../../../icons/Dashboard_Icons/iPhone_1.png" alt="Phone" />
          <div>
            <p>iPhone 13</p>
            <p>₦730,000.00</p>
          </div>
          <p>12 in Stock</p>
        </div>
        <Sender text="Hello, I want to make enquiries about your product" />
        <Receipient text="Hello Janet, thank you for reaching out" />
        <Receipient text="What do you need to know?" />

        <Date text="Today" />

        <Sender text="I want to know if the proce is negaotiable. i need about 2 units" />
        {/* Type Section */}
        <TypeMsg />
      </section>
      <div className={`${styles.showContact} ${styles.addArrow}`}>
        {!toggleContactList && (
          <FaArrowRight onClick={() => setToggleContactList(true)} />
        )}
        {toggleContactList && (
          <FaTimes
            style={{ color: "red" }}
            onClick={() => setToggleContactList(false)}
          />
        )}
      </div>
    </main>
  );
};

export default Chat;
