import styles from "../../styles/conversations/typemsg.module.css";

const TypeMsg = () => {
  return (
    <section className={styles.section}>
      <textarea type="text" placeholder="Your Message" />
      <img src="../../../icons/Conversation_icons/Plus.png" alt="" />
      <img src="../../../icons/Conversation_icons/Smiley.png" alt="" />
      <img src="../../../icons/Conversation_icons/Send.png" alt="" />
    </section>
  );
};

export default TypeMsg;
