import styles from "../../styles/conversations/header.module.css";

const Header = () => {
  return (
    <main className={styles.main}>
      <p>Conversations with Customers</p>
      <button>New Message</button>
    </main>
  );
};

export default Header;
