export const sideBarData = [
  {
    id: 1,
    icon: "../icons/Dashboard.png",
    sideBarContent: "Dashboard",
    link: "/",
  },
  {
    id: 2,
    icon: "../icons/Orders.png",
    sideBarContent: "Orders",
    link: "/orders",
  },
  {
    id: 3,
    icon: "../icons/Customers.png",
    sideBarContent: "Customers",
    link: "/customers",
  },
  {
    id: 4,
    icon: "../icons/Inventory.png",
    sideBarContent: "Inventory",
    link: "/inventory",
  },
  {
    id: 5,
    icon: "../icons/Conversations.png",
    sideBarContent: "Conversations",
    link: "/conversations",
  },
  {
    id: 6,
    icon: "../icons/Setting.png",
    sideBarContent: "Settings",
    link: "/settings",
  },
];

export const eventData = [
  {
    id: 1,
    icon: "../icons/Dashboard_Icons/event_1.png",
    text1: "Sales",
    text2: "Volume",
    figure1: "₦4,000,000.00",
    figure2: "450",
    increment2: "+20.00%",
  },
  {
    id: 2,
    icon: "../icons/Dashboard_Icons/event_2.png",
    text1: "Customers",
    text2: "Active",
    figure1: "1,250",
    figure2: "1,180",
    increment: "+15.80%",
    increment2: "85%",
  },
  {
    id: 3,
    icon: "../icons/Dashboard_Icons/event_3.png",
    text1: "All Orders",
    text2: "Pending",
    text3: "Completed",
    figure1: "450",
    figure2: "5",
    figure3: "445",
  },
];

export const recentOrderData = [
  {
    id: 1,
    status: "pending",
  },
  {
    id: 2,
    status: "completed",
  },
  {
    id: 3,
    status: "pending",
  },
  {
    id: 4,
    status: "completed",
  },
  {
    id: 5,
    status: "completed",
  },
  {
    id: 6,
    status: "completed",
  },
  {
    id: 7,
    status: "pending",
  },
  {
    id: 8,
    status: "pending",
  },
  {
    id: 9,
    status: "pending",
  },
];

export const chatData = [
  {
    id: 1,
    name: "Jane Doe",
    status: "New",
  },
  {
    id: 2,
    name: "Janet Adebayo",
    status: "New",
  },
  {
    id: 3,
    name: "Kunle Adekunle",
    status: "New",
  },
  {
    id: 4,
    name: "Jane Doe",
    status: "2",
  },
  {
    id: 5,
    name: "Janet Adebayo",
    // status: "",
  },
  {
    id: 6,
    name: "Kunle Adekunle",
    // status: "",
  },
  {
    id: 7,
    name: "Jane Doe",
    // status: "",
  },
  {
    id: 8,
    name: "Janet Adebayo",
    // status: "",
  },
  {
    id: 9,
    name: "Kunle Adekunle",
    // status: "",
  },
];
