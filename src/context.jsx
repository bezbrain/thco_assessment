import React, { useContext, useState } from "react";
import { recentOrderData } from "./data";

const AppContext = React.createContext();

export const AppProvider = ({ children }) => {
  const [isModal, setIsModal] = useState(false); // Logout Modal
  const [isLogout, setIsLogout] = useState("Logout"); // To dynamically change the Logout content to Login
  const [toggleLogout, setToggleLogout] = useState(false); // To show Login/Logout Modal container
  const [toggleContactList, setToggleContactList] = useState(false); //To toggle contact list at tablet and mobile screens

  // Dynamically get the pending figures
  const newOrder = recentOrderData.filter((each) => each.status === "pending");

  return (
    <AppContext.Provider
      value={{
        newOrder,
        isModal,
        setIsModal,
        isLogout,
        setIsLogout,
        toggleLogout,
        setToggleLogout,
        toggleContactList,
        setToggleContactList,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

export const useGlobalContext = () => {
  return useContext(AppContext);
};
