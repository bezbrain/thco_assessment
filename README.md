# THCO Assessment

(https://thco-assessment.netlify.app/)

This repository contains lines of codes used to build a UI design and based on my experience, I implemented the major and necessary functionalities to enhance user experience.

### With this project, the following were implemented:

##### 1) Responsive development for all screen sizes (mobile, tablet and desktop)

##### 2) Functionalities Like:

- Logout Modal
- Toggle between Login and Logout
- Pop up notification when a user logs out
- Dynamically navigate to the dashboard when a user logs out
- Routes for all pages and provided designs for the available ones
- Dynamically change active nav item

##### 3) Most practised folder structure

##### 4) Meticulous way of grouping files and folders

##### 5) Used Context API as a state management tool

##### 6) Used comments where necessary

##### 7) Used CSS modules so that CSS files can be easily tracked

##### 8) And many more...

The link to access the finished work is as provided above, and you can also click [here](https://thco-assessment.netlify.app/) to access it.
